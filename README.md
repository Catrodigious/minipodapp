# MiniPodApp

A small re-creation of a multi-printer management system app. More functionality still needs to be added.

## Notes

This project is still incomplete. It requires Mongoose models and server-side communication with OctoPrint among other things.

### Prerequisites
Bower
NPM

## Installing

1. Check out the MiniPodApp: `git clone https://bitbucket.org/Catrodigious/minipodapp.git`
2. Change directories into the MiniPodApp directory: `cd minipodapp/`
3. Download all of the npm requirements by using: `npm install`
4. Download all of the bower requirements by using: `bower install`
5. Start the app using: `npm start`
6. Open a web browser and go to `http://localhost:3000/`

## Authors

* **Cat C.** - [Catrodigious](https://bitbucket.org/catrodigious)

## License

This project is licensed under the AGPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
