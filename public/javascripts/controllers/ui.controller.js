
app.controller("MainUIController", ['$scope', '$q', '$http', 'FileInteractionFactory', function($scope, $q, $http, fileInteractionFactory){

	$scope.printers = [];
	$scope.selectedPrinters = [];
	$scope.uploadedFiles = [];
	$scope.singleSelectedFile = {};
	$scope.actionLog = [];
	$scope.dateToday = new Date();
	$scope.disabledStartButton = true;
	
	let initialize = function(){
		var options = {
			method: 'GET',
			url: '/printers',
		}	
		$http(options).then(function successCallback(sData){
			let pList = sData.data.printers;
			$scope.printers = pList;
			$scope.actionLog.push({message: 'Initialized Print Pod Software', timeStamp: Date.now()});
		}, function errCallback(err){
			console.error(err);
		});
	};
	
	
	let getSelectedPrinters = function(){
		$scope.selectedPrinters = [];
		
		$scope.printers.find((o, i) => {
			if (o.selected === true) {
				$scope.selectedPrinters.push($scope.printers[i]);
			};
		});
		
		$scope.printers.find((o, i) => {
			if (o.selected === false){
				var item = $scope.printers[i];
				var index = $scope.selectedPrinters.indexOf(item);
				
				if (index !== -1){
					$scope.selectedPrinters.splice(index);
				};
			};
		});
	};	
	
	
	$scope.togglePrinterSelect = function(printer){
		let index = $scope.printers.indexOf(printer);
		$scope.printers[index].selected = !printer.selected;
		
		getSelectedPrinters();
	};


	$scope.toggleFileSelect = function(file){
		let index = $scope.uploadedFiles.indexOf(file);
		let isSelected = !file.selected;	
		$scope.uploadedFiles[index].selected = isSelected;
		
		if (isSelected == true){
			$scope.singleSelectedFile = file;
			
			for (var i=0; i < $scope.uploadedFiles.length; i++){
				if (i !== index){
					$scope.uploadedFiles[i].selected = false;	
				};
			};
		}else{
			$scope.singleSelectedFile = '';
		};
	};


	$scope.printerSelectStyleToggle = function(printer){
		if (printer.selected == true){
			return {'background-color': 'rgba(204,236,255,0.6)'}
		} else if (printer.selected == false){
			return {'background-color': 'white'};
		};
	};
	
	
	$scope.fileSelectStyleToggle = function(file){
		let index = $scope.uploadedFiles.indexOf(file);
		
		if (index == -1) return;
		
		if (file.selected === true){
			return {'background-color': 'rgba(204,236,255,0.6)'}
		}else{
			return {'background-color': 'white'}
		};
	};
	
	
	$scope.listAllSelectedPrinters = function(){
		getSelectedPrinters();
	};
	
	
	$scope.enableJobStart = function(){
		if (Object.keys($scope.singleSelectedFile).length > 0 && $scope.selectedPrinters.length > 0){
			return false;
		}else{
			return true;
		};
	};
	
	
	$scope.$watch('newFileObject', function(newV, oldV){
		if (newV && newV !== null && newV.length > 0){
			for (var i=0; i < newV.length; i++){
				let fileSize = newV[i].size / 1000000;
				let fileObj = newV[i];
				let uploadTime = Date.now();
				let message = "Uploaded file: " + String(fileObj.name);
				let timeStamp = Date.now();
									
				fileObj.mbSize = fileSize.toFixed(2);
				fileObj.uploadTime = uploadTime;
				
				if (newV.length == 1){
					fileObj.selected = true;
					$scope.singleSelectedFile = fileObj;
				}else if (newV.length > 1){
					fileObj.selected = false;
				};
				
				$scope.uploadedFiles.push(fileObj);
				$scope.actionLog.push({'message': message, 'timeStamp': timeStamp})
			};
		};
	});
	
	initialize();
}]);

