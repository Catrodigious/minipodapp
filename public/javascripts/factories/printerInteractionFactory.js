app.factory('PrinterInteractionFactory', ['$http', '$q', function($http, $q){
	var options = {
		method: 'GET',
		url: 'http://localhost:3000/printers',
	}

	$http(options).then(function successCallback(sData){
		console.log("Success: ", sData);
	}, function errCallback(err){
		console.log("Error:", err)
	})

}]);