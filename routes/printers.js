var express = require('express');
var router = express.Router();
var path = require('path');
var basePath = path.join(__dirname, '../all_printers.json');
var fs = require('fs');
var PrinterAPI = require(__dirname, '')


// GET home page. 

router.get('/', function(req, res, next) {
	try{
		fs.readFile(basePath, 'utf8', function(err, data){
			if (err) throw err;
			if (data) res.send(JSON.parse(data));
		});
	}catch(err){
		res.send(err);
	};
	
});

module.exports = router;
